<?php
    include('server.php');
    error_reporting(E_ALL ^ E_NOTICE);
?>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-commerce - John Marson Vida</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
    .container {
          padding: 80px 120px;
      }
      .container-content {
          text-align: justify;
      }

      .container-content a {
          color: #000;
      }
    </style>
</head>

<form action="server.php" method="POST">
  <div class="col-md-8">
    <br>
    <h4>E-Commerce Activity</h4>
    <br>
    <div class="row">
      <div class="col-sm-6 form-group">
        <input class="form-control" id="name" name="name" placeholder="Your Name" type="text" required>
      </div>
      <div class="col-sm-6 form-group">
        <input class="form-control" id="contact" name="contact" placeholder="Your Contact No" type="text" required>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-12 form-group">
        <button class="btn pull-right" type="submit" name="submit">Send</button>
      </div>
    </div>
    <div class="msg">
      <?php if(isset($_SESSION['msg'])): ?>
        <div class="label label-danger">
          <?php
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
          ?>
        </div>
        <?php endif ?>
    </div>
  </div>
</form>
</html>
