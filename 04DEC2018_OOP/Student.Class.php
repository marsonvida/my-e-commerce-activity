<?php

	class Student
	{
		var $_studentNo = "";
		var $_firstName = "";
		var $_lastName = "";
		var $_birthDay = "";
		var $_gender = "";
		var $_yearLevel = "";
		var $_course = "";
		var $_address = "";

		//ctor
		public function __construct() 
		{
			//default
		}	

		public function __construct1($_studentNo) 
		{
			$this->_studentNo = $_studentNo;
		}	
		
		//setter
		public function setStudentInfo($_studentNo,$_firstName, $_lastName, $_birthDay, $_gender, $_yearLevel, $_course, $_address) 
		{ 
			$this->_studentNo = $_studentNo;
			$this->_firstName = $_firstName;
			$this->_lastName = $_lastName;
			$this->_birthDay = $_birthDay;
			$this->_gender = $_gender;
			$this->_yearLevel = $_yearLevel;
			$this->_course = $_course;
			$this->_address = $_address;
		}

		//getters
		public function getStudentNo() 
		{ 
			return $this->_studentNo;
		}
		public function getStudentFirstName() 
		{ 
			return $this->_firstName;
		}
		public function getStudentLastName() 
		{ 			
			return $this->_lastName;
		}
		public function getStudentBday() 
		{ 
			return $this->_birthDay;
		}
		public function getStudentGender() 
		{ 			
			return $this->_gender;
		}
		public function getStudentLvl() 
		{ 
			return $this->_yearLevel;
		}
		public function getStudentCourse() 
		{ 		
			return $this->_course;
		}
		public function getStudentAdd() 
		{ 
			return $this->_address;
		}
	}
		
?>